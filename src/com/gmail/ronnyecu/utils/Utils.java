package com.gmail.ronnyecu.utils;

import java.util.regex.Pattern;

public class Utils {
    public static void main(String[] args) {
        String[] url = {
                "http://domain.es:8080/events/01-03-2020/",
                "https://domain.cat/noticies/2020-02-20/",
                "domain.cat/news/asas",
                "domain.es/eventes/asas?param",
                "domain.es/news/evento&param",
                "http://domain.com:8080/news/20-06-2019?utg=aqawdasdmiau",
                "http://domain.com:8080/privacitat",
                "http://domain.com:8080/home/",
        };
        String ca = "domain.cat";
        String en = "domain.com";
        String es = "domain.es";
        String events_CA = "events";
        String events_ES = "eventos";
        String events_EN = "events";
        String news_CA = "noticies";
        String news_ES = "noticias";
        String news_EN = "news";

        String protocol = "(^https?://(w{3}\\.)?)?";
        String domain = "(" + ca + "|" + es + "|" + en + ")";
        String port = "(:\\d*)?";
        String section = "/(" + events_CA + "|" + events_ES + "|" + events_EN + "|" + news_CA + "|" + news_ES + "|" + news_EN + ")/";
        String subSection = "[^(?&)]+/?$";
        Pattern pattern = Pattern.compile(protocol + domain + port + section + subSection);

        for (String s : url) {
            System.out.println(String.format("%s: %b", s, pattern.matcher(s).matches()));
        }

    }
}
